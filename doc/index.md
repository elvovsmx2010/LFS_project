# Linux From Scratch
## Eugeny Lvov
### Introducción
**Linux From Scratch** tiene como propósito enseñar y explicar el funcionamiento de sistema operativo GNU LINUX. El Proyecto LFS te da la oportunidad de seguir la instalación paso a paso y darte la información suficiente para que una vez instalado el sistema operativo puedas seguir avanzando por tus propios medios.
#### A quién va dirigido
Esta documentación va dirigida a personas que ya tienen alguna experiencia con GNU LINUX. Personas que tienen pensado o están realizando el proyecto LFS y quieren profundizar su conocimientos. 
#### Qué encontrarás en este documento
El propósito de este documento es explicar el funcionamiento de una forma general del sistema operativo GNU LINUX enfocando en los puntos más importantes como: Disco Ram inicial Initrd, demonio Init, X Server etc…
A pesar de que el servicio de procesos Systemd ya ha sido implantado en varias distribuciones actuales este documento está enfocado en el antiguo demonio de servicios Init para simplificar el entendimiento. 
Los ejemplos mostrados no están enfocados en ninguna arquitectura en particular. 

### Antes de empezar
Construir un sistema operativo GNU LINUX de cero no es una tarea rápida, dependerá de tus conocimientos actuales y el hardware del ordenador en el que se decida hacer la instalación. Hay que tener en cuenta que aunque se haga el seguimiento paso a paso de la documentación LFS saldrán errores lo cual implicará un esfuerzo y tiempo adicional. 

### Contenido
* Introducción
* Preparación del entorno de trabajo
* Compilar e instalar un paquete
* Construcción del sistema temporal
* Construcción del sistema final
* Preparación del arranque: System V (INIT)
* Preparación del arranque: Kernel y drivers
* Preparación del arranque: initramfs
* Preparación del arranque: Grub
* Servidor gráfico X 

### Ayuda adicional
* https://docs.fedoraproject.org/en-US/index.html - Documentación de Fedora es una de las más completas para GNU LINUX
* https://wiki.archlinux.org - Wiki de ArchLinux es conocida por profundizar muchos en los aspectos técnicos de las instalaciones y usos de ordenes.
* http://www.server-world.info/en/ - En Server World se explica la instalación de diferentes servicios para distribuciones basadas en RedHat y Debian que pueden servir de mucha ayuda.