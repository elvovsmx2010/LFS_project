# 5. Construcción del sistema final
Una vez instalado sistema temporal se procede a instalar el sistema final. Este es el momento en el que dejaremos a un lado las herramientas del sistema anfitrión y empecemos a trabajar con las herramientas del sistema temporal.

Para poder usar herramientas del sistema temporal independientemente del sistema anfitrión usaremos la herramienta `chroot`. 

La funcionalidad de `chroot` es enjaular (limitar el entorno) permitiendo configurar un directorio como raíz del sistema de ficheros para un proceso y sus hijos. 

Para poder acceder a nuestro sistema temporal con `chroot` primero debemos montar el **Kernel virtual**. Para ello primero montamos los directorios y nodos de dispositivos necesarios:

    mkdir -pv $LFS/{dev,proc,sys,run} # Directorios virtuales
    mknod -m 600 $LFS/dev/console c 5 1 # Nodo de dispositivo
    mknod -m 666 $LFS/dev/null c 1 3 # Nodo de dispositivo

Ahora montamos le sistemas de archivos virtuales del kernel:

    mount -v --bind /dev $LFS/dev
    mount -vt devpts devpts $LFS/dev/pts -o gid=5,mode=620 # 5=tty
    mount -vt proc proc $LFS/proc
    mount -vt sysfs sysfs $LFS/sys
    mount -vt tmpfs tmpfs $LFS/run
> Más información sobre sistema de archivo virtuales de Kernel en el apartado de “Preparación del arranque: Kernel y drivers”.

Una vez montado el **Kernel virtual** procedemos a utilizar la herramienta `chroot` para enjaular nuestro sistema temporal como la raíz. Necesitamos ejecutar la siguiente orden como usuario privilegiado. 

    chroot "$LFS" /tools/bin/env -i \ 
        HOME=/root                  \
        TERM="$TERM"                \
        PS1='\u:\w\$ '              \
        PATH=/bin:/usr/bin:/sbin:/usr/sbin:/tools/bin \

>Explicación de la orden: enjaular `$LFS` con `chroot`, eliminar variables del entorno heredados de sistema anfitrión y establecer variables propias. Ponga atención en la última ruta del $PATH **“/tools/bin”** indica que a partir de ahora usaremos herramientas (ejecutables binarios) del sistema temporal. 

A partir de aquí una vez enjaulados en el sistema temporal procedemos con la instalación del sistema final. 
Primero crearemos el árbol de directorios del sistema GNU LINUX con permisos y enlaces adecuados. 

    mkdir -pv /{bin,boot,etc/{opt,sysconfig},home,lib/firmware,mnt,opt}
    mkdir -pv /{media/{floppy,cdrom},sbin,srv,var}
    install -dv -m 0750 /root
    install -dv -m 1777 /tmp /var/tmp
    mkdir -pv /usr/{,local/}{bin,include,lib,sbin,src}
    mkdir -pv /usr/{,local/}share/{color,dict,doc,info,locale,man}
    mkdir -v  /usr/{,local/}share/{misc,terminfo,zoneinfo}
    mkdir -v  /usr/libexec
    mkdir -pv /usr/{,local/}share/man/man{1..8}
    
    case $(uname -m) in
     x86_64) ln -sv lib /lib64
             ln -sv lib /usr/lib64
             ln -sv lib /usr/local/lib64 ;;
    esac
    
    mkdir -v /var/{log,mail,spool}
    ln -sv /run /var/run
    ln -sv /run/lock /var/lock
    mkdir -pv /var/{opt,cache,lib/{color,misc,locate},local}
    
    ln -sv /tools/bin/{bash,cat,echo,pwd,stty} /bin
    ln -sv /tools/bin/perl /usr/bin
    ln -sv /tools/lib/libgcc_s.so{,.1} /usr/lib
    ln -sv /tools/lib/libstdc++.so{,.6} /usr/lib
    sed 's/tools/usr/' /tools/lib/libstdc++.la > /usr/lib/libstdc++.la
    ln -sv bash /bin/sh

Algunos programas usan rutas fijas “hard-wired paths” a programas que aún no existen, para satisfacer dichos ejecutables crearemos los enlaces mostrados anteriormente que más tarde serán reemplazados con ejecutables binarios reales al ser instalados posteriormente. 

Debemos crear `/etc/passwd`, `/etc/group`, `/etc/sysconfig/ifconfig.ethX`, `/etc/resolv.conf`, `/etc/hostname`, `/etc/profile` etc... manualmente:
* [createfile.html](http://www.linuxfromscratch.org/lfs/view/stable/chapter06/createfiles.html)
* [network.html](http://www.linuxfromscratch.org/lfs/view/stable/chapter07/network.html)
* [profile.html](http://www.linuxfromscratch.org/lfs/view/stable/chapter07/profile.html)

El resto de instalación del sistema final es muy parecido al que se ha hecho anteriormente con sistema temporal solo que esta vez instalaremos más software creando un sistema operativo más completo y totalmente independiente.