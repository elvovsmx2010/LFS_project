# 1. Introducción
La idea principal de construcción de LFS se basa en disponer de un GNU LINUX instalado (o LiveCD) en el ordenador en el que se va a realizar la instalación. Se aconseja escoger una *distribución estable* para evitar cualquier tipo de complicación innecesaria. 

Antes de explicar el procedimiento me gustaría definir unos términos:

* **Sistema anfitrión** - Sistema operativo GNU LINUX en el que se hará la instalación LFS.
* **Sistema temporal** - Sistema de ficheros GNU LINUX que contiene herramientas básicas para la construcción de LFS. 
* **Sistema final** - Sistema operativo GNU LINUX que se construirá con las herramientas de Sistema temporal.

El procedimiento de la instalación LFS es el siguiente:
1. Configurar el sistema anfitrión para que cumpla los requisitos para la construcción de sistema temporal.
2. Construir el sistema temporal.
3. A partir de sistema temporal construir sistema final.